; ***** COPPER MACROS, 05-Dec-87 / 26-Jul-89 CHW *************************

cmove:		MACRO
		  DC.W	((\2)&$01fe)		; Zielregister
		  DC.W	\1
		ENDM

cmovel:		MACRO
		  DC.W	((\2)&$01fe)		; Zielregister Hi-Word
		  DC.W	(\1)/$10000
		  DC.W	((\2)&$01fe)+2		; Zielregister Lo-Word
		  DC.W	(\1)&$ffff		
		ENDM

cwait:		MACRO
		  IFNE	(NARG)-2
		    FAIL cwait: bad arg count
		  ENDC
		  DC.W	((\1)!1)&$ffff		; Rasterzeile und -Spalte
		  DC.W	((\2)&$fffe)!$8000	; Compare-Enable-Maske
		ENDM

cwaitv:		MACRO
		  IFNE	(NARG)-1
		    FAIL cwaitv: bad arg count
		  ENDC
		  DC.W	(((\1)<<8)!1)&$ffff	; Nur Zeile berücksichtigen
		  DC.W	$fffe			; Compare-Enable-Maske
		ENDM

cend:		MACRO
		  DC.W	$ffff,$fffe
		ENDM

cskip:		MACRO
		  DC.W	(\1)!1			; Rasterzeile und -Spalte
		  DC.W	(\2)!$8001		; Compare-Enable-Maske
		ENDM

