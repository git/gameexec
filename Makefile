##########################################################################
#                                                                        #
#   Makefile für das mega-geniale Game-Exec-Betriebssystem von CHW :-)   #
#                                                                        #
#   Created: 18-May-89 CHW                  Last update: 23-Feb-91 CHW   #
#                                                                        #
##########################################################################


CC	= SC
CFLAGS	=

ASM	= Genam
AFLAGS	= -l -iINCLUDE: -eDEBUG=1

LD	= Slink
LFLAGS	= ADDSYM NOICONS



.c.o:
	$(CC) $(CFLAGS) $*.c

.S.or:
	-@Delete $@
	$(ASM) $(AFLAGS) $*.S -o$*.or -eRAMVERSION=1

.S.os:
	-@Delete $@
	$(ASM) $(AFLAGS) $*.S -o$*.os -eSYSTEM=1

.S.o:
	-@Delete $@
	$(ASM) $(AFLAGS) $*.S -o$*.o -eDISKVERSION=1



DISKMODS =	Exec.o  Memory.o  LoadSeg.o  Keyboard.o  CDisk.o  FileIO.o  RAMLib.o\
 PPDecrunch.o  RawDoFmt.o  DrawBob.o  Rnd.o  FFS.o  EndLabel.o

RAMMODS  =	Exec.or Memory.or LoadSeg.or Keyboard.or CDisk.or FileIO.or RAMLib.or\
 PPDecrunch.or RawDoFmt.or DrawBob.or Rnd.or FFS.or EndLabel.or

SYSMODS  =	Exec.os Memory.os LoadSeg.os Keyboard.os SysCDisk.os FileIO.os RAMLib.os\
 PPDecrunch.os RawDoFmt.os DrawBob.os Rnd.os CDTV.os EndLabel.os



All:		Track0 Start SysStart FinalBooter

$(RAMMODS):	myexec.i
$(DISKMODS):	myexec.i
$(SYSMODS):	myexec.i
DrawBob.o:	DrawBob.i


RAMExec.S:	$(RAMMODS)
		-@Delete $@
		$(LD) $(LFLAGS) FROM $(RAMMODS) TO T:__exectmp LIB LIB:small.lib
		AbsLoad >NIL: -a0 -o T:__exectmp2 T:__exectmp
		HexDump -L -X _ExecModuleStart -Y _ExecModuleEnd -O$@ T:__exectmp2
		@Delete T:__exectmp T:__exectmp2

Track0:		$(DISKMODS)
		-@Delete $@
		$(LD) $(LFLAGS) FROM $(DISKMODS) TO T:__exectmp LIB LIB:small.lib
		AbsLoad >NIL: -a0 -o T:__exectmp2 T:__exectmp
		Join BootBlock T:__exectmp2 as Track0
		@Delete T:__exectmp T:__exectmp2 QUIET

Start:		start.o vbr.o rawdofmt.o ramexec.o
		-@Delete $@
		$(LD) $(LFLAGS) FROM CCLIB:argsstartup20.o start.o vbr.o rawdofmt.o ramexec.o TO $* \
 LIB CCLIB:ch.lib LIB:sc.lib LIB:small.lib

SysStart:	sysstart.o $(SYSMODS)
		-@Delete $@
		$(LD) $(LFLAGS) FROM CCLIB:argsstartup20.o Sysstart.o $(SYSMODS) TO $* \
 LIB CCLIB:ch.lib LIB:sc.lib LIB:small.lib

FinalBooter:	FinalBooter.o
		-@Delete $@
		$(LD) $(LFLAGS) FROM cclib:TinyStartup.o $*.o TO T:__exectmp LIB cclib:ch.lib LIB:small.lib
		AbsLoad -©cb -o$@ T:__exectmp
		@Delete T:__exectmp

Disk-Validator:	FinalBooter.S
		-@Delete $@
		$(ASM) FinalBooter.S -o$@ -eBCPL=1


clean:
		Delete *.o *.os *.or Track0 Start SysStart FinalBooter Disk-Validator ramexec.s


install:
		Copy MyExec.i DrawBob.i INCUSR: CLONE
		Copy Start C+:Proprietary/ CLONE
		Copy SysStart C+:Proprietary/ CLONE


dist:		Start Track0
		-@Delete Exec.LHA
		@lha -x -a a Exec.LHA Start Track0 MyExec.i DrawBob.i
		@lha -x -a v Exec.LHA

srcdist:	Start BootBlock CDisk.o CDisk.r
		-@Delete ExecSource.LHA
		@lha -x -a a ExecSource.LHA *.S *.c
		@lha -x -a d ExecSource.LHA ramexec.s
		@lha -x -a a ExecSource.LHA BootBlock CDisk.o CDisk.r *.i Makefile
		@lha -x -a v ExecSource.LHA
