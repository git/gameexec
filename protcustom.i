**************************************************************************
**                                                                      **
**   protcustom.i  -  Offsets der Amiga Custom-Chips, Base falsch!      **
**                                                                      **
**************************************************************************
**                                                                      **
**   Modification History                                               **
**   --------------------                                               **
**                                                                      **
**   20-Jan-88  Created this file from relcustom.i                      **
**   26-Apr-89  sprdatb corrected (was 8, should be 6)                  **
**   29-Jun-89  Ist jetzt auch Genim2-kompatibel                        **
**   16-Sep-90  CUSTOMOFFSET muss jetzt extern definiert werden         **
**                                                                      **
**************************************************************************

custom:		EQU	$dff000-CUSTOMOFFSET	; Start der Custom-Chips

bltddat:	EQU	$000+CUSTOMOFFSET
dmaconr:	EQU	$002+CUSTOMOFFSET
vposr:		EQU	$004+CUSTOMOFFSET
vhposr:		EQU	$006+CUSTOMOFFSET
dskdatr:	EQU	$008+CUSTOMOFFSET
joy0dat:	EQU	$00A+CUSTOMOFFSET
joy1dat:	EQU	$00C+CUSTOMOFFSET
clxdat:		EQU	$00E+CUSTOMOFFSET
adkconr:	EQU	$010+CUSTOMOFFSET
pot0dat:	EQU	$012+CUSTOMOFFSET
pot1dat:	EQU	$014+CUSTOMOFFSET
potinp:		EQU	$016+CUSTOMOFFSET
serdatr:	EQU	$018+CUSTOMOFFSET
dskbytr:	EQU	$01A+CUSTOMOFFSET
intenar:	EQU	$01C+CUSTOMOFFSET
intreqr:	EQU	$01E+CUSTOMOFFSET
dskpt:		EQU	$020+CUSTOMOFFSET
dsklen:		EQU	$024+CUSTOMOFFSET
dskdat:		EQU	$026+CUSTOMOFFSET
refptr:		EQU	$028+CUSTOMOFFSET
vposw:		EQU	$02A+CUSTOMOFFSET
vhposw:		EQU	$02C+CUSTOMOFFSET
copcon:		EQU	$02E+CUSTOMOFFSET
serdat:		EQU	$030+CUSTOMOFFSET
serper:		EQU	$032+CUSTOMOFFSET
potgo:		EQU	$034+CUSTOMOFFSET
joytest:	EQU	$036+CUSTOMOFFSET
strequ:		EQU	$038+CUSTOMOFFSET
strvbl:		EQU	$03A+CUSTOMOFFSET
strhor:		EQU	$03C+CUSTOMOFFSET
strlong:	EQU	$03E+CUSTOMOFFSET

bltcon0:	EQU	$040+CUSTOMOFFSET
bltcon1:	EQU	$042+CUSTOMOFFSET
bltafwm:	EQU	$044+CUSTOMOFFSET
bltalwm:	EQU	$046+CUSTOMOFFSET
bltcpt:		EQU	$048+CUSTOMOFFSET
bltbpt:		EQU	$04C+CUSTOMOFFSET
bltapt:		EQU	$050+CUSTOMOFFSET
bltdpt:		EQU	$054+CUSTOMOFFSET
bltsize:	EQU	$058+CUSTOMOFFSET

bltcmod:	EQU	$060+CUSTOMOFFSET
bltbmod:	EQU	$062+CUSTOMOFFSET
bltamod:	EQU	$064+CUSTOMOFFSET
bltdmod:	EQU	$066+CUSTOMOFFSET

bltcdat:	EQU	$070+CUSTOMOFFSET
bltbdat:	EQU	$072+CUSTOMOFFSET
bltadat:	EQU	$074+CUSTOMOFFSET

dsksync:	EQU	$07E+CUSTOMOFFSET

cop1lc:		EQU	$080+CUSTOMOFFSET
cop2lc:		EQU	$084+CUSTOMOFFSET
copjmp1:	EQU	$088+CUSTOMOFFSET
copjmp2:	EQU	$08A+CUSTOMOFFSET
copins:		EQU	$08C+CUSTOMOFFSET
diwstrt:	EQU	$08E+CUSTOMOFFSET
diwstop:	EQU	$090+CUSTOMOFFSET
ddfstrt:	EQU	$092+CUSTOMOFFSET
ddfstop:	EQU	$094+CUSTOMOFFSET
dmacon:		EQU	$096+CUSTOMOFFSET
clxcon:		EQU	$098+CUSTOMOFFSET
intena:		EQU	$09A+CUSTOMOFFSET
intreq:		EQU	$09C+CUSTOMOFFSET
adkcon:		EQU	$09E+CUSTOMOFFSET

aud:		EQU	$0A0+CUSTOMOFFSET
aud0:		EQU	$0A0+CUSTOMOFFSET	; Basis fuer Audio-Kanal 0
aud1:		EQU	$0B0+CUSTOMOFFSET	; Basis fuer Audio-Kanal 1
aud2:		EQU	$0C0+CUSTOMOFFSET	; Basis fuer Audio-Kanal 2
aud3:		EQU	$0D0+CUSTOMOFFSET	; Basis fuer Audio-Kanal 3

audlc:		EQU	$00
audlen:		EQU	$04
audper:		EQU	$06
audvol:		EQU	$08
auddat:		EQU	$0A

bplpt:		EQU	$0E0+CUSTOMOFFSET	; Basis fuer BitPlane Pointer
bpl1pt:		EQU	$0E0+CUSTOMOFFSET
bpl2pt:		EQU	$0E4+CUSTOMOFFSET
bpl3pt:		EQU	$0E8+CUSTOMOFFSET
bpl4pt:		EQU	$0EC+CUSTOMOFFSET
bpl5pt:		EQU	$0F0+CUSTOMOFFSET
bpl6pt:		EQU	$0F4+CUSTOMOFFSET

bplcon0:	EQU	$100+CUSTOMOFFSET
bplcon1:	EQU	$102+CUSTOMOFFSET
bplcon2:	EQU	$104+CUSTOMOFFSET
bpl1mod:	EQU	$108+CUSTOMOFFSET
bpl2mod:	EQU	$10A+CUSTOMOFFSET
bpldat:		EQU	$110+CUSTOMOFFSET

sprpt:		EQU	$120+CUSTOMOFFSET	; Basis fuer Sprite-Pointers
spr:		EQU	$140+CUSTOMOFFSET	; Basis fuer Sprites

sprpos:		EQU	$00+CUSTOMOFFSET
sprctl:		EQU	$02+CUSTOMOFFSET
sprdataa:	EQU	$04+CUSTOMOFFSET
sprdatab:	EQU	$06+CUSTOMOFFSET

color:		EQU	$180+CUSTOMOFFSET

beamcon0:	EQU	$1DC+CUSTOMOFFSET	; Fatter Agnus only

