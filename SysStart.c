/****************************************************************************
**                                                                         **
**  SysStart.c  -  Startet Exec MIT Betriebssystem                         **
**                                                                         **
*****************************************************************************
**                                                                         **
**   Modification History                                                  **
**   --------------------                                                  **
**                                                                         **
**   24-Feb-91  CHW  Created this file from Start.S                        **
**   29-Jun-20  CHW  Aufgeräumt, öffnet jetzt Screen damit's mit UAE läuft **
**                                                                         **
****************************************************************************/

#include <proto/exec.h>
#include <exec/io.h>
#include <exec/memory.h>
#include <exec/execbase.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <graphics/gfxbase.h>
#include <hardware/custom.h>
#include <hardware/dmabits.h>
#include <hardware/intbits.h>
//#include <resources/cia.h>
#include <string.h>				/* z.B. für __builtin_memcpy */
#include <dos.h>

#include <chlib.h>


char ident[]         = "$VER: SysStart 5.0 by Christian A. Weber (" __DATE__ ")";
char CLI_Template[]  = "CS=CHIPSIZE/N,FS=FASTSIZE/N,MOD/K,PRI/N";
char CLI_Help[]      = "Usage: SysStart [CHIPSIZE kbytes] [FASTSIZE kbytes] [MOD name] [PRI pri]";

extern __regargs void Exit(LONG);
extern void RawPrintfFunc(char *, ...);

extern struct ExecBase *SysBase;
extern struct GfxBase *GfxBase;

extern BPTR StdErr;						/* Standard Error output stream */
extern struct Custom far volatile custom;

extern void __asm __far InitExec(
	register __d0 LONG,  register __d1 LONG,
	register __d2 LONG,  register __d3 LONG,
	register __a0 void *,register __a1 void *,
	register __a2 void *,register __a3 void *);


struct
{
	LONG *ChipSizePtr;
	LONG *FastSizePtr;
	char *ModName;
	LONG *PriPtr;
} argv;


UBYTE *chipbase;		/* Startadresse des CHIP-RAMs für Exec */
ULONG chipsize;			/* Grösse des CHIP-RAMs für Exec */

UBYTE *fastbase;		/* Startadresse des FAST-RAMs für Exec */
ULONG fastsize;			/* Grösse des FAST-RAMs für Exec */

struct Screen *screen;	/* Wir öffnen einen Customscreen um Picasso- oder UAE-Screenmodes abzuschalten */

UWORD dmaconsave, intenasave;
ULONG attnflags, vblankfreq, sysbplcon0;
BYTE oldtaskpri;


/***************************************************************************/
/* Alles freigeben, back to DOS */

__saveds void ExitRoutine(void)
{
	int i;
	custom.color[0] = 0x173;		/* Grünlich */

	RawPrintfFunc("SysStart: ExitRoutine()\r\n");
	printf("done.\n");

	WaitBlit();
	//DisownBlitter();

	custom.cop1lc = (ULONG)GfxBase->copinit;	/* Bild wieder einschalten */

	custom.dmacon = 0x7FFF;
	custom.dmacon = dmaconsave | DMAF_SETCLR;	/* Original dmacon zurückholen */

	custom.intena = 0x7FFF;
	custom.intena = intenasave | INTF_SETCLR;	/* Dito mit intena */

	for(i=0; i<8; ++i)
		custom.spr[i].dataa = custom.spr[i].datab = 0;

	RemakeDisplay();
	SetTaskPri(SysBase->ThisTask, oldtaskpri);

	if (screen)
	{
		CloseScreen(screen);
		screen = NULL;
	}

	if (fastbase)
	{
		FreeMem(fastbase, fastsize);
		fastbase = NULL;
	}

	if (chipbase)
	{
		FreeMem(chipbase, chipsize);
		chipbase = NULL;
	}

	CloseIntLib();
	CloseGfxLib();

	Exit(RETURN_OK);
}


/***************************************************************************/
/* Hauptprogramm */

LONG Main(LONG arglen, char *argline)
{
	char *module = "MainPrg";
	LONG taskpri = 0;

	OpenGfxLib();
	OpenIntLib();

	printf("\n%s\n", ident+6);

	AllocMem(0x40000000,0);		/* Flushlibs */
	AllocMem(0x40000000,0);

	if (argv.ChipSizePtr)
	{
		chipsize = *argv.ChipSizePtr << 10;	/* KBytes to Bytes */
		if(chipsize < 100000)
		{
			Puts("CHIP size must be > 100K, please try again!");
			return RETURN_ERROR;
		}
	}
	else
	{
		chipsize = (AvailMem(MEMF_CHIP|MEMF_LARGEST) - 30000) & ~0xff;
		if(chipsize > 1000000)
			chipsize = (3*chipsize)/4;
	}


	if (argv.FastSizePtr)
	{
		fastsize = *argv.FastSizePtr << 10;
	}
	else
	{
		fastsize = (AvailMem(MEMF_FAST|MEMF_LARGEST) - 50000) & ~0xff;
		if(fastsize > 1000000)
			fastsize = (3*fastsize)/4;
	}

	if (argv.ModName)
		module = argv.ModName;

	if (argv.PriPtr)
		taskpri = *argv.PriPtr;


	if (!(screen = OpenScreenTags(NULL, SA_Width,320, SA_Height,200, SA_Depth,1, SA_Type,CUSTOMSCREEN, TAG_DONE)))
	{
		Puts("Can't open screen!");
		ExitRoutine();
	}


	if (chipbase = AllocMem(chipsize, MEMF_CHIP|MEMF_CLEAR))
	{
		printf("Chip RAM: $%08lx (%luK)\n", chipbase, chipsize >> 10);

		if (fastbase = AllocMem(fastsize, MEMF_FAST|MEMF_CLEAR))
		{
			printf("Fast RAM: $%08lx (%luK)\n", fastbase, fastsize >> 10);
		}
		else
		{
			fastsize = 0;
			Puts("Not enough FAST RAM available");
		}

		//OwnBlitter();
		WaitBlit();

		oldtaskpri = SetTaskPri(SysBase->ThisTask,taskpri);

		/* System-Status für MyExec merken/retten etc. */

		dmaconsave = custom.dmaconr;		// &~DMAF_SPRITE;
		intenasave = custom.intenar;		// &~INTF_INTEN;
		attnflags  = SysBase->AttnFlags;
		vblankfreq = SysBase->VBlankFrequency;
		sysbplcon0 = GfxBase->system_bplcon0;

		custom.color[0] = 0xF00;		/* Bildschirm rot */

		RawPrintfFunc("\n%s: Running %s ...\r\n", ident+6, module);

		InitExec(	attnflags,			/* D0 */
					sysbplcon0,			/* D1 */
					vblankfreq,			/* D2 */
					(LONG)module,		/* D3 :  MainPrg-Name */
					chipbase,			/* A0 :  CHIP-Startadresse */
					chipbase+chipsize,	/* A1 :  CHIP-Grösse */
					fastbase,			/* A2 :  FAST-Startadresse */
					fastbase+fastsize	/* A3 :  FAST-Endadresse */
		);

		ExitRoutine();
		/* not reached */

	}
	else printf("Can't get %luK CHIP RAM!\n",chipsize/1024);

	return RETURN_WARN;

}
