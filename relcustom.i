*********************************************************************
**                                                                 **
**  relcustom.i  -  Alle Offsets der Amiga Custom-Chips            **
**                                                                 **
**  Created: 27-Oct-87 CHW             Last update: 16-Jan-88 CHW  **
**                                                                 **
*********************************************************************

custom:		EQU	$dff000		; Startadresse der Custom-Chips

bltddat:	EQU	$000
dmaconr:	EQU	$002
vposr:		EQU	$004
vhposr:		EQU	$006
dskdatr:	EQU	$008
joy0dat:	EQU	$00A
joy1dat:	EQU	$00C
clxdat:		EQU	$00E
adkconr:	EQU	$010
pot0dat:	EQU	$012
pot1dat:	EQU	$014
potinp:		EQU	$016
serdatr:	EQU	$018
dskbytr:	EQU	$01A
intenar:	EQU	$01C
intreqr:	EQU	$01E
dskpt:		EQU	$020
dsklen:		EQU	$024
dskdat:		EQU	$026
refptr:		EQU	$028
vposw:		EQU	$02A
vhposw:		EQU	$02C
copcon:		EQU	$02E
serdat:		EQU	$030
serper:		EQU	$032
potgo:		EQU	$034
joytest:	EQU	$036
strequ:		EQU	$038
strvbl:		EQU	$03A
strhor:		EQU	$03C
strlong:	EQU	$03E

bltcon0:	EQU	$040
bltcon1:	EQU	$042
bltafwm:	EQU	$044
bltalwm:	EQU	$046
bltcpt:		EQU	$048
bltbpt:		EQU	$04C
bltapt:		EQU	$050
bltdpt:		EQU	$054
bltsize:	EQU	$058

bltcmod:	EQU	$060
bltbmod:	EQU	$062
bltamod:	EQU	$064
bltdmod:	EQU	$066

bltcdat:	EQU	$070
bltbdat:	EQU	$072
bltadat:	EQU	$074

dsksync:	EQU	$07E

cop1lc:		EQU	$080
cop2lc:		EQU	$084
copjmp1:	EQU	$088
copjmp2:	EQU	$08A
copins:		EQU	$08C
diwstrt:	EQU	$08E
diwstop:	EQU	$090
ddfstrt:	EQU	$092
ddfstop:	EQU	$094
dmacon:		EQU	$096
clxcon:		EQU	$098
intena:		EQU	$09A
intreq:		EQU	$09C
adkcon:		EQU	$09E

aud:		EQU	$0A0
aud0:		EQU	$0A0		; Basis fuer Audio-Kanal 0
aud1:		EQU	$0B0		; Basis fuer Audio-Kanal 1
aud2:		EQU	$0C0		; Basis fuer Audio-Kanal 2
aud3:		EQU	$0D0		; Basis fuer Audio-Kanal 3

audlc:		EQU	$00
audlen:		EQU	$04
audper:		EQU	$06
audvol:		EQU	$08
auddat:		EQU	$0A

bplpt:		EQU	$0E0		; Basis fuer BitPlane Pointer
bpl1pt:		EQU	$0E0
bpl2pt:		EQU	$0E4
bpl3pt:		EQU	$0E8
bpl4pt:		EQU	$0EC
bpl5pt:		EQU	$0F0
bpl6pt:		EQU	$0F4

bplcon0:	EQU	$100
bplcon1:	EQU	$102
bplcon2:	EQU	$104
bpl1mod:	EQU	$108
bpl2mod:	EQU	$10A
bpldat:		EQU	$110

sprpt:		EQU	$120		; Basis fuer Sprite-Pointers
spr:		EQU	$140		; Basis fuer Sprites

sprpos:		EQU	$00
sprctl:		EQU	$02
sprdata:	EQU	$04
sprdatb:	EQU	$08

color:		EQU	$180

htotal      EQU   $1c0
hsstop      EQU   $1c2
hbstrt      EQU   $1c4
hbstop      EQU   $1c6
vtotal      EQU   $1c8
vsstop      EQU   $1ca
vbstrt      EQU   $1cc
vbstop      EQU   $1ce
sprhstrt    EQU   $1d0
sprhstop    EQU   $1d2
bplhstrt    EQU   $1d4
bplhstop    EQU   $1d6
hhposw      EQU   $1d8
hhposr      EQU   $1da
beamcon0    EQU   $1dc
hsstrt      EQU   $1de
vsstrt      EQU   $1e0
hcenter     EQU   $1e2
diwhigh     EQU   $1e4


VARVBLANK	EQU	$1000	; Variable vertical blank enable 
LOLDIS		EQU	$0800	; long line disable 
CSCBLANKEN	EQU	$0400	; redirect composite sync 
VARVSYNC	EQU	$0200	; Variable vertical sync enable 
VARHSYNC	EQU	$0100	; Variable horizontal sync enable 
VARBEAM		EQU	$0080	; variable beam counter enable 
DISPLAYDUAL	EQU	$0040	; use UHRES pointer and standard pointers 
DISPLAYPAL	EQU	$0020	; set decodes to generate PAL display 
VARCSYNC	EQU	$0010	; Variable composite sync enable 
CSBLANK		EQU	$0008	; Composite blank out to CSY* pin 
CSYNCTRUE	EQU	$0004	; composite sync true signal 
VSYNCTRUE	EQU	$0002	; vertical sync true 
HSYNCTRUE	EQU	$0001	; horizontal sync true 

	** new defines for bplcon0
USE_BPLCON3	EQU	1
	** new defines for bplcon2

BPLCON2_ZDCTEN		EQU	(1<<10) ; colormapped genlock bit 
BPLCON2_ZDBPEN		EQU	(1<<11) ; use bitplane as genlock bits 
BPLCON2_ZDBPSEL0	EQU	(1<<12) ; three bits to select one 
BPLCON2_ZDBPSEL1	EQU	(1<<13) ; of 8 bitplanes in 
BPLCON2_ZDBPSEL2	EQU	(1<<14) ; ZDBPEN genlock mode 

	** defines for bplcon3 register 

BPLCON3_EXTBLNKEN	EQU	(1<<0)	; external blank enable 
BPLCON3_EXTBLKZD	EQU	(1<<1)	; external blank ored into trnsprncy 
BPLCON3_ZDCLKEN		EQU	(1<<2)	; zd pin outputs a 14mhz clock
BPLCON3_BRDNTRAN	EQU	(1<<4)	; border is opaque 
BPLCON3_BRDNBLNK	EQU	(1<<5)	; border is opaque 

