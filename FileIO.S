
		IDNT	FileIO
		SECTION	text,CODE


		INCLUDE	"MyExec.i"

		XREF	ProcessNextRequest

	IFND SYSTEM
		XREF	GetD7A4A5
	ENDC

	IFD RAMVERSION
		XREF	ProcessFFSPacket	; Für ramdrive.device
	ENDC

		XDEF	ReadFileFunc		; 'Datei' lesen
		XDEF	WriteFileFunc		; 'Datei' schreiben

		XDEF	LoadFileFunc		; Speicher reservieren & File lesen
		XDEF	LoadFastFileFunc	; FAST alloc   & File lesen

		XDEF	SendPacketFunc		; Asynchronen Lese-Request schicken

		XDEF	ReplyPacket		; Internal helper for CDisk.S/SysCDisk.S



***************************************************************************
**                                                                       **
**   R E A D F I L E  -  Datei von Disk an angegebene Adresse laden      **
**                                                                       **
**   Parameter :  D0.L :  DiskAdresse (Disk/Track/Offset) der Datei      **
**                A0.L :  Ladeadresse                                    **
**                                                                       **
**   Resultat  :  nix                                                    **
**                                                                       **
***************************************************************************

ReadFileFunc:	movem.l	d1/a0-a1,-(SP)
		moveq	#0|DPF_REPLYBYTE,d1		; LESEN
		bra.s	APost				; Paket aufgeben


***************************************************************************
**                                                                       **
**   W R I T E F I L E  -  Existierende 'Datei' auf Disk überschreiben   **
**                                                                       **
**   Parameter :  D0.L :  DiskAdresse (Disk/Track/Offset) der Datei      **
**                A0.L :  Adresse der Daten für das File                 **
**                                                                       **
**   Resultat  :  nix                                                    **
**                                                                       **
***************************************************************************

WriteFileFunc:	movem.l	d1/a0-a1,-(SP)
		move.b	#DPF_WRITE|DPF_REPLYBYTE,d1	; SCHREIBEN
		bra.s	APost				; Paket aufgeben


***************************************************************************
**                                                                       **
**   L O A D F I L E  -  Speicher reservieren, Datei von Disk lesen      **
**                                                                       **
**   Parameter :  D0.L :  DiskAdresse (Disk/Track/Offset) der Datei      **
**                                                                       **
**   Resultat  :  D0.L :  Adresse des Files, 0 if error                  **
**                Z-Bit:  gelöscht wenn OK, gesetzt wenn Error           **
**                                                                       **
***************************************************************************

LoadFileFunc:	movem.l	d1/a0-a1,-(SP)
		moveq	#DPF_REPLYBYTE|DPF_ALLOCMEM,d1	; Packet Flags
		bra.s	APost				; Paket aufgeben

LoadFastFileFunc:
		movem.l	d1/a0-a1,-(SP)
		moveq	#DPF_REPLYBYTE|DPF_ALLOCFASTMEM,d1 ; Packet Flags
	;;	bra.s	APost				   ; Paket aufgeben

APost:		lea	-dp_SIZEOF-2(SP),SP	; DiskPacket erstellen
		move.l	a0,dp_Address(SP)	; Ladeadresse eintragen
		movea.l	SP,a0			; A0 :  Packet
		move.l	d0,dp_FileName(a0)	; Dateinamen eintragen
		lea	dp_SIZEOF(SP),a1	; A1 :  End-Flag
		clr.b	(a1)			; löschen
		move.l	a1,dp_Reply(a0)
		move.b	d1,dp_Flags(a0)		; DPF_REPLYBYTE [|DPF_WRITE] usw.
		bsr	SendPacketFunc
1$:
		tst.b	(a1)			; Warten bis File geladen/geschrieben
		beq.s	1$
		move.l	dp_Address(SP),d0	; Resultat: Adresse
		lea	dp_SIZEOF+2(SP),SP	; DiskPacket freigeben
		movem.l	(SP)+,d1/a0-a1
		rts


***************************************************************************
**                                                                       **
**   S E N D P A C K E T  -  Asynchronen Read-Request aufgeben           **
**                                                                       **
**   Parameter :  A0.L :  Zeiger auf struct DiskPacket                   **
**                                                                       **
**   Resultat  :  nix                                                    **
**                                                                       **
***************************************************************************

SendPacketFunc:
	IFD SYSTEM
		movem.l	a0-a1,-(SP)
	ELSE
		movem.l	d7/a0-a1/a4-a5,-(SP)
		bsr	GetD7A4A5		; Für ProcessNextRequest() in CDisk.S (nicht in SysCDisk.S)
	ENDC
		movea.l	a0,a1			; Packet
		lea	meb_DiskList(a6),a0
		jsr	meb_AddTail(a6)		; Packet anhängen
		bsr	ProcessNextRequest	; System ankicken
	IFD SYSTEM
		movem.l	(SP)+,a0-a1
	ELSE
		movem.l	(SP)+,d7/a0-a1/a4-a5
	ENDC
		rts


***************************************************************************

	*** Packet (A0) beantworten

ReplyPacket:	movem.l	d0/a0-a2,-(SP)
		movea.l	a0,a2			; A2 :  Packet
		moveq	#0,d0
		move.b	dp_Flags(a2),d0

	;;;	SMSG	<"ReplyPacket($%08lx), flags=$%02ld">,a2,d0

	IFD CRUNCH
		btst	#DPB_CRUNCHED,d0
		beq.s	1$
		movea.l	dp_Address(a2),a0	; A0 :  Start der gecrunchten Daten
		move.l	dp_FileSize(a2),d0	; D0 :  File-Länge gecruncht
		move.l	(a0),dp_FileSize(a2)	; Echte Länge für User
		SMSG	<"Decrunching %ld to %ld bytes at $%08lx">,d0,(a0),a0
		bsr	PPDecrunch		; File decrunchen
1$:
	ENDC
		movea.l	dp_Reply(a2),a1		; A1 :  User's Reply-Adresse
		btst	#DPB_REPLYHANDLER,d0
		beq.s	2$
	;;	SMSG	<"Calling reply handler $%08lx">,a1
		movea.l	a2,a0			; A0 :  Packet für User
		jsr	(a1)			; ReplyHandler aufrufen
		bra.s	99$			; --->
2$:
		btst	#DPB_REPLYBYTE,d0
		beq.s	3$
		st.b	(a1)			; ReplyByte setzen
	;;	bra.s	99$			; --->
3$:
99$:		movem.l	(SP)+,d0/a0-a2
		rts


		END
