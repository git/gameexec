***************************************************************************
**                                                                       **
**   zeropage.i - Alle Interrupt- und Exception-Vektoren                 **
**                                                                       **
**   Modification History                                                **
**   --------------------                                                **
**                                                                       **
**   06-May-89 CHW  Created this file!                                   **
**                                                                       **
***************************************************************************

   STRUCTURE ZeroPage,0

	APTR	ResetSSP		; 0
	APTR	ResetVector		; 1
	APTR	BusErrorVector		; 2
	APTR	AddrErrVector		; 3
	APTR	IllegalVector		; 4
	APTR	DivZeroVector		; 5
	APTR	ChkVector		; 6
	APTR	TrapVVector		; 7
	APTR	PrivVector		; 8
	APTR	TraceVector		; 9
	APTR	LineAVector		; A
	APTR	LineFVector		; B
	APTR	Reserved0C		; C
	APTR	ProtocolVector		; D
	APTR	FormatVector		; E
	APTR	NotIntVector		; F
	STRUCT	Reserved10,8*4		; 10-17
	APTR	WrongIntVector		; 18
	APTR	Level1Vector		; 19
	APTR	Level2Vector		; 1A
	APTR	Level3Vector		; 1B
	APTR	Level4Vector		; 1C
	APTR	Level5Vector		; 1D
	APTR	Level6Vector		; 1E
	APTR	Level7Vector		; 1F
	STRUCT	TrapVectors,16*4	; 20-2F
